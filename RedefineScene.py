from PyQt5.QtCore import Qt, QPointF, QRectF, QLineF
from PyQt5.QtGui import QPen, QColor
from PyQt5.QtWidgets import QGraphicsScene


class Scene(QGraphicsScene):
	def __init__(self, main):
		super(Scene, self).__init__()
		self.main = main

		self.start_point = None
		self.init_pos = []  # init position
		self.end_pos = []  # end position
		# 建立畫筆
		self.pen = QPen(Qt.red)
		self.pen.setWidth(5)

		self.current_rect_item = None
		self.current_line_item = None

	def reset(self):

		self.clear()
		self.init_pos = []
		self.end_pos = []
		self.current_rect_item = None
		self.current_line_item = None

	def mousePressEvent(self, event):
		# 繪圖模式下
		if self.main.drawing() and self.main.count == 0:
			if event.button() == Qt.LeftButton:

				self.start_point = event.scenePos()
				self.update()
				self.init_pos = [int(event.scenePos().x()), int(event.scenePos().y())]
				# print(f'初始座標{self.init_pos}')

	def mouseMoveEvent(self, event):
		# 繪圖模式下

		if self.main.drawing() and self.main.count == 0:
			if event.buttons() == Qt.LeftButton:

				self.update()
				self.end_pos = [int(event.scenePos().x()), int(event.scenePos().y())]
				# print(f'結束座標{self.end_pos}')

				self.main.labelPos.setText('%.d,%.d' % (event.scenePos().x(), event.scenePos().y()))

				# 判斷座標成立並且起始與結束沒有在同一點上
				if self.init_pos != [] and self.end_pos != []:
					if self.init_pos[1] != self.end_pos[1] or self.init_pos[0] != self.end_pos[0]:
						'''addRect 和 addLine 來創建新的矩形和線條 setRect 和 setLine 更新它們的位置和大小。'''
						# 先判斷 rect_item 是否存在
						if not self.current_rect_item:
							self.current_rect_item = self.addRect(QRectF(self.start_point, event.scenePos()),
																  QPen(QColor("red"), 5, Qt.DotLine))
						else:
							self.current_rect_item.setRect(QRectF(self.start_point, event.scenePos()))
							self.current_rect_item.update()

						# 先判斷 line_item 是否存在
						if not self.current_line_item:
							self.current_line_item = self.addLine(QLineF(self.start_point, event.scenePos()),
																  QPen(QColor("red"), 5, Qt.SolidLine))
						else:
							self.current_line_item.setLine(QLineF(self.start_point, event.scenePos()))
							self.current_line_item.update()

	def mouseReleaseEvent(self, event):
		# 繪圖模式下
		if self.main.drawing() and self.main.count == 0:
			if event.button() == Qt.LeftButton:

				# 判斷座標成立並且起始與結束沒有在同一點上
				if self.init_pos != [] and self.end_pos != []:
					if self.init_pos[1] != self.end_pos[1] or self.init_pos[0] != self.end_pos[0]:

						print(f'end position: {self.end_pos} not equal to init position {self.init_pos}')
						rect = QRectF(QPointF(*self.init_pos), QPointF(*self.end_pos))

						# 使用矩形的寬與高絕對值來判斷 ，是否大於1 ---> 門檻越低，越嚴謹
						if abs(rect.width()) > 1 and abs(rect.height()) > 1:
							print(f'rectangle width: {rect.width()}more than 100, height: {rect.height()} more the 100')

							# when init position > end position exchange each other
							if self.init_pos[0] > self.end_pos[0]:
								self.end_pos[0], self.init_pos[0] = max(self.init_pos[0], self.end_pos[0]), min(
									self.init_pos[0],
									self.end_pos[0])

							if self.init_pos[1] > self.end_pos[1]:
								self.end_pos[1], self.init_pos[1] = max(self.init_pos[1], self.end_pos[1]), min(
									self.init_pos[1],
									self.end_pos[1])
							

							# 不管成不成立，在滑鼠鬆開後將在過程中的 item 刪除
							if self.current_rect_item:
								self.removeItem(self.current_rect_item)
							if self.current_line_item:
								self.removeItem(self.current_line_item)

							# 最後將矩形畫上
							self.addRect(rect, self.pen)

							self.update()

							'''繪圖數量 = 1 變回編輯模式 變回原鼠標'''
							self.main.count = 1

							self.main.mode = self.main.EDIT
							self.main.viewOriginal.unsetCursor()

					else:
							# 不成立就清空座標
							self.init_pos, self.end_pos = [], []

				# 起始與結束座標在同一點上也是清空座標
				elif self.init_pos[1] == self.end_pos[1] or self.init_pos[0] == self.end_pos[0]:
					self.init_pos, self.end_pos = [], []
					self.clear()
