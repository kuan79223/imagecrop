import glob
import os
import sys

import cv2
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import Qt, QThread, pyqtSignal, QRect, QSize, QRectF
from PyQt5.QtGui import QImage, QPixmap, QPainter
from PyQt5.QtWidgets import QHeaderView, QAbstractItemView, QGraphicsView, QLabel, QSizePolicy, QTableWidgetItem, \
    QFileDialog

from Dialog import dialog
from Dialog.dialog import Dialog
import ShowImg
import RedefineScene
import Crop_view_ui


class CropTask(QThread):
    def __init__(self, main):
        super(CropTask, self).__init__()
        self.main = main

    def run(self):

        for filedir in self.main.crop_imgs:
            filename, ext = os.path.splitext(filedir)

            img = cv2.imread(filedir)
            crop = img[self.main.scene.init_pos[1]: self.main.scene.end_pos[1], self.main.scene.init_pos[0]:self.main.scene.end_pos[0]]
            out_path = os.path.join(self.main.crop_path, filename.split('\\')[-1] + '_crop' + ext)
            # 將影像寫入專案路徑
            cv2.imwrite(out_path, crop)


class LoadImgTask(QThread):
    SIGNAL_FINISHED = pyqtSignal()

    def __init__(self, main):
        super(LoadImgTask, self).__init__()
        self.main = main
        self.running = True
        self.SIGNAL_FINISHED.connect(self.finished)
        self.images = []

    def run(self):

        for path, subdir, name in os.walk(self.main.source):
            for img_path in glob.glob(os.path.join(path, '*.png')) + glob.glob(os.path.join(path, '*.jpg')) + glob.glob(os.path.join(path, '*.jpeg')):

                self.images.append(img_path)
        # print(self.images)

        self.running = True
        row = 0
        # while self.running:
        for filename in self.images:

            pixmap = QPixmap(filename)
            # 取得影像的解析度
            img = cv2.imread(filename)
            # cv2.imshow('img', img)
            # cv2.waitKey(1)
            height, width = img.shape[:2]
            pixels = f'{width} x {height}'
            name = filename.split('\\')[-1]
            content = f'{name}\n\n {pixels}'
            self.main.SIGNAL_LOAD_IMG.emit(row, pixmap, content)
            # print(f'發射信號{row},{pixmap},{content}')
            # print('執行緒')
            row += 1

        self.SIGNAL_FINISHED.emit()


    # 這個是讓外部強制關閉的方法
    def break_off(self):
        self.running = False

    # 結束信號槽
    def finished(self):
        self.running = False
        print('結束信號槽')


class CropView(QtWidgets.QWidget, Crop_view_ui.Ui_Form):
    SIGNAL_LOAD_IMG = pyqtSignal(int, object, str)

    def __init__(self):
        super(CropView, self).__init__()
        self.setupUi(self)
        self.EDIT, self.DRAW = range(2)

        self.source = ''
        self.result = ''
        self.mode = None
        self.img = None
        self.pixmap = None
        self.crop_imgs = []
        self.count = 0

        self.crop_path = ''
        self.SIGNAL_LOAD_IMG.connect(self.set_table)

        # graphics_view 設定
        self.scrollArea.setWidgetResizable(True)  # 讓 scrollArea自適應
        self.viewOriginal.setMouseTracking(True)
        self.viewOriginal.setRenderHints(QPainter.Antialiasing | QPainter.SmoothPixmapTransform)
        self.viewOriginal.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.viewOriginal.setResizeAnchor(QGraphicsView.AnchorUnderMouse)
        # 按鈕
        self.btn_open_dir.clicked.connect(self.open_source)
        self.btn_save_dir.clicked.connect(self.save_result)
        self.btn_box.clicked.connect(self.draw_box)
        self.btn_view.clicked.connect(self.show_view)
        self.btn_undo.clicked.connect(self.undo)
        self.btn_crop.clicked.connect(self.crop_img)
        self.checkBox.stateChanged.connect(self.on_checkbox_changed)
        self.btn_save_coordinate.clicked.connect(self.save_bndbox)
        self.btn_load_coordinate.clicked.connect(self.load_bndbox)


        # 實力化 python
        self.load_img_task = LoadImgTask(self)
        self.crop_task = CropTask(self)
        self.dialog = None
        self.scene = RedefineScene.Scene(self)
        self.show_img = ShowImg.ShowImg(self)
        self.dialog_view = None
         
        # table widget設定 -----------------------------------------------------------------
        # 設定欄位數量
        self.tableWidget.setColumnCount(2)
        self.tableWidget.setColumnWidth(1, 300)  # 設定索引 1 欄位寬度
        # 固定表格視窗大小
        self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Fixed)
        self.tableWidget.verticalHeader().setSectionResizeMode(QHeaderView.Fixed)
        self.tableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)  # 選擇整列數據
        # 隱藏 標題欄與列
        self.tableWidget.verticalHeader().setVisible(False)
        self.tableWidget.horizontalHeader().setVisible(False)
        self.tableWidget.setShowGrid(False)  # 移除表格線

        self.tableWidget.itemClicked.connect(self.on_item_clicked)  # table widget 觸發事件
        # table widget設定 -----------------------------------------------------------------
        # scene 縮放
        self.btn_zoom_ori.clicked.connect(self.fit_zoom)
        self.btn_zoom_in.clicked.connect(self.zoomin)
        self.btn_zoom_out.clicked.connect(self.zoomout)
        self.zoom_factor = 1.0
        self.zoom_step = 0.25  # set the zoom step is 0.25
        self.zoom_range = [0.1, 2]  # set the smallest  is 10 percent, set the biggest is 200 percent

    # --------------------------------------------------------------------------------------------

    def wheelEvent(self, event):

        # 計算縮放因子並限制縮放範圍
        zoom_factor = 1 + self.zoom_step * (event.angleDelta().y() / 120)
        new_zoom_factor = self.zoom_factor * zoom_factor

        if new_zoom_factor > self.zoom_range[1]:

            zoom_factor = self.zoom_range[1] / self.zoom_factor

        elif new_zoom_factor < self.zoom_range[0]:

            zoom_factor = self.zoom_range[0] / self.zoom_factor

        # update factor and view
        self.zoom_factor *= zoom_factor
        self.viewOriginal.scale(zoom_factor, zoom_factor)
        # 更新顯示 label 縮放比例
        self.labelScale.setText(f'{int(self.zoom_factor * 100)}%')

    def fit_zoom(self):
        self.viewOriginal.resetTransform()
        self.viewOriginal.fitInView(self.scene.itemsBoundingRect(), QtCore.Qt.KeepAspectRatio)
        self.labelScale.setText(f'{100}%')

    def zoomin(self):

        zoom_step = 0.25  # set the zoom step is 0.25

        # Compute zoom factor and constrain to zoom range
        zoom_factor = 1 + zoom_step
        new_zoom_factor = self.zoom_factor * zoom_factor

        if new_zoom_factor > self.zoom_range[1]:
            zoom_factor = self.zoom_range[1] / self.zoom_factor

        # update factor and view
        self.zoom_factor *= zoom_factor
        self.viewOriginal.scale(zoom_factor, zoom_factor)

        # update display label this zoom scale
        self.labelScale.setText(f"{int(self.zoom_factor * 100)}%")

    def zoomout(self):

        zoom_step = 0.25  # set the zoom step is 0.25

        # Compute zoom factor and constrain to zoom range
        zoom_factor = 1 - zoom_step
        new_zoom_factor = self.zoom_factor * zoom_factor

        if new_zoom_factor < self.zoom_range[0]:
            zoom_factor = self.zoom_range[0] / self.zoom_factor

        # update factor and view
        self.zoom_factor *= zoom_factor
        self.viewOriginal.scale(zoom_factor, zoom_factor)

        # update display label this zoom scale
        self.labelScale.setText(f"{int(self.zoom_factor * 100)}%")

    def getZoomFactor(self, delta):
        return self.zoom_factor ** (delta / 120)


    def open_source(self):

        self.source = QFileDialog.getExistingDirectory(self, "選擇來源路徑", 'D:/', options=QFileDialog.ShowDirsOnly)

        if self.source != '' and self.result != '':
            self.load_img_task.start()
            self.lb_open_path.setText(self.source)
            self.lb_save_path.setText(self.result)
        else:
            print('取消選擇\n')
            return

    def save_result(self):

        self.result = QFileDialog.getExistingDirectory(self, "選擇儲存路徑", 'D:/', options=QFileDialog.ShowDirsOnly)

        if self.result != '' and self.source != '':
            self.load_img_task.start()
            self.lb_open_path.setText(self.source)
            self.lb_save_path.setText(self.result)
            print('儲存的檔案路徑為:', self.result)
        else:
            print('取消選擇\n')
            return

    # 觸發 table widget 的事件
    def on_item_clicked(self, item):
        self.count = 0
        self.checkBox.setChecked(False)  # 點擊 table widget 解除 check box 的全選
        self.scene.clear()  # 清空 scene , 以便下一張圖片被觸發

        row = item.row()  # 獲得列的內容
        cell_value = self.tableWidget.item(row, 1).text()
        # print(cell_value.split('\n')[0])
        for path, subdir, files in os.walk(self.source):

            if cell_value.split('\n')[0] in files:
                image = os.path.join(path, cell_value.split('\n')[0])
                print(image)
                self.crop_imgs.append(image)
                self.img = cv2.imread(image)

                self.height, self.width = self.img.shape[:2]
                qimg = QImage(bytes(self.img), self.width, self.height, 3 * self.width, QImage.Format_BGR888)
                self.pixmap = QPixmap(qimg)

                self.scene.addPixmap(self.pixmap)
                self.viewOriginal.setScene(self.scene)
                self.viewOriginal.fitInView(self.scene.itemsBoundingRect(), QtCore.Qt.KeepAspectRatio)

    # 判斷繪圖數量是否為0，才變成繪圖模式，十字線
    def draw_box(self):
        # 每次點擊清空scene座標系
        self.scene.init_pos = []
        self.scene.end_pos = []

        if self.count == 0 and self.img is not None:
            self.mode = self.DRAW

            if self.drawing():
                self.viewOriginal.setCursor(Qt.CrossCursor)
            elif self.editing():
                self.viewOriginal.unsetCursor()  # 原來的鼠標

    # 繪圖狀態
    def drawing(self):
        return self.mode == self.DRAW

    # 編輯狀態
    def editing(self):
        return self.mode == self.EDIT

    # 檢查裁切的影像
    def show_view(self):

        if self.editing() and self.count == 1:
            self.show_img.display()
            self.show_img.exec()

    # btn undo event => 編輯模式 和 繪圖數量 == 1，才能清空繪圖數量與 scene，並且重新將影像圖放進 scene 中
    def undo(self):

        if self.editing() and self.count == 1:

            self.count = 0
            self.scene.reset()
            self.scene.addPixmap(self.pixmap)
            self.viewOriginal.setScene(self.scene)
            self.viewOriginal.fitInView(self.scene.itemsBoundingRect(), QtCore.Qt.KeepAspectRatio)
            self.labelScale.clear()

    # 核取方塊事件
    def on_checkbox_changed(self, state):

        if state == Qt.Checked:
            self.tableWidget.selectAll()  # select all rows
            rows = self.tableWidget.rowCount()
            for row in range(rows):
                item = self.tableWidget.item(row, 1)
                filename = item.text().split('\n')[0]

                for path, subdir, files in os.walk(self.source):
                    if filename in files:
                        image = os.path.join(path, filename)

                        self.crop_imgs.append(image)
        else:
            self.tableWidget.clearSelection()

    # crop img event => 裁切之前會確認座標是否存在
    def crop_img(self):

        if self.source != '' and self.result != '':

            if self.editing():
                # 視窗彈出就會把座標塞進要裁切的暫存中
                if self.scene.init_pos and self.scene.end_pos:
                    self.dialog = dialog.Dialog()
                    self.dialog.show_msg('確定裁切大小!!')
                    self.dialog.exec()

                    # 按下確認裁切
                    if self.dialog.result == 1:
                        self.crop_path = os.path.join(self.result, 'crop')
                        if not os.path.exists(self.crop_path):
                            os.makedirs(self.crop_path)

                        self.crop_task.start()

                else:
                    self.dialog = dialog.Dialog()
                    self.dialog.show_msg('尚未選擇你要裁切的範圍')
                    self.dialog.exec()

        else:
            self.dialog = dialog.Dialog()
            self.dialog.show_msg('檢查來源路徑與儲存路徑')
            self.dialog.exec()

    # 讀取座標配方，並加入裁切暫存與座標
    def load_bndbox(self):

        if self.source != '' and self.result != '':

            recipe_txt, _ = QFileDialog.getOpenFileName(self, "選擇配方檔案", 'D:/', 'File type(*.txt)')

            if recipe_txt != '':
                with open(recipe_txt, 'r', encoding='utf-8') as f:
                    temp = f.readline().split(',')
                    print('讀取配方坐標系', temp)
                    # 判斷裁切的條件為座標是否成立，而裁切會以全域變數的座標裁切
                    self.scene.init_pos = [int(temp[0]), int(temp[1])]
                    self.scene.end_pos = [int(temp[2]), int(temp[3])]
                    # 判斷 scene 是否有影像
                    if self.img is not None:

                        rect = cv2.rectangle(self.img.copy(), (self.scene.init_pos[0], self.scene.init_pos[1]),
                                             (self.scene.end_pos[0], self.scene.end_pos[1]), (0, 0, 255), 5)
                        self.height, self.width = rect.shape[:2]
                        qimg = QImage(bytes(rect), self.width, self.height, 3 * self.width, QImage.Format_BGR888)
                        pixmap = QPixmap(qimg)
                        self.scene.addPixmap(pixmap)

                        self.count = 1
                        self.mode = self.EDIT
            else:
                self.dialog = dialog.Dialog()
                self.dialog.show_msg('配方不存在')
                self.dialog.exec()

        else:
            self.dialog = dialog.Dialog()
            self.dialog.show_msg('檢查來源路徑與儲存路徑')
            self.dialog.exec()


    # 將資料顯示在table 上
    def set_table(self, row, pixmap, content):

        # 判斷路徑內是否有檔案
        if os.listdir(self.source):

            self.tableWidget.setRowCount(row + 1)

            self.tableWidget.setRowHeight(row, 100)  # 動態生成每個 row 高
            label1 = QLabel()
            label1.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
            label1.setPixmap(pixmap.scaled(100, 100, aspectRatioMode=QtCore.Qt.KeepAspectRatio))  # 讓影像自適應

            self.tableWidget.setCellWidget(row, 0, label1)
            # -------------------------------------------------------------------------------
            # 以文本的方式創建在tableWidget
            item = QTableWidgetItem(content)
            self.tableWidget.setItem(row, 1, item)

            self.tableWidget.horizontalHeader().setSectionResizeMode(0, QHeaderView.Interactive)
            # print(f'在table widget增加了一筆資料{content}')

    def save_bndbox(self):

        if self.source != '' and self.result != '':

            recipe_txt = os.path.join(self.crop_path, 'recipe.txt')
            coordinate = ','.join(map(str, self.scene.init_pos + self.scene.end_pos))
            print('儲存座標系', coordinate)
            with open(recipe_txt, 'w', encoding='utf-8') as f:
                f.write(coordinate)

        else:
            self.dialog = dialog.Dialog()
            self.dialog.show_msg('檢查來源路徑與儲存路徑')
            self.dialog.exec()


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    form = CropView()
    form.showMaximized()
    sys.exit(app.exec_())
