import sys
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QDialog, QSizePolicy

import ShowImg_ui


class ShowImg(QDialog, ShowImg_ui.Ui_Dialog):

	def __init__(self, main):
		super(ShowImg, self).__init__()
		self.setupUi(self)
		self.setFixedSize(600, 400)
		self.main = main

	def display(self):

		print(f'show_image，起始座標容器{self.main.scene.init_pos}\n,結束座標容器{self.main.scene.end_pos}')
		box_img = self.main.img[self.main.scene.init_pos[1]:self.main.scene.end_pos[1], self.main.scene.init_pos[0]:self.main.scene.end_pos[0]]

		height, width = box_img.shape[:2]
		qimg = QImage(bytes(box_img), width, height, 3 * width, QImage.Format_BGR888)

		self.label.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
		self.label.setPixmap(QPixmap(qimg).scaled(600, 400, aspectRatioMode=QtCore.Qt.KeepAspectRatio))


if __name__ == '__main__':
	app = QtWidgets.QApplication(sys.argv)
	form = ShowImg()
	form.show()
	sys.exit(app.exec_())
